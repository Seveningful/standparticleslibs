package fr.standblock.hub.updater;
public class UtilTime
{
  public static boolean elapsed(long from, long required)
  {
    return System.currentTimeMillis() - from > required;
  }
}
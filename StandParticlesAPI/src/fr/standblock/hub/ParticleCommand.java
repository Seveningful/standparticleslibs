package fr.standblock.hub;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.standblock.hub.particles.Type;
import net.minecraft.server.v1_8_R3.EnumParticle;

public class ParticleCommand extends Command {

	protected ParticleCommand(String name) {
		super(name);

	}
	@Override
	public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
		if(alias.equalsIgnoreCase("test")) {
			if(args.length == 1) {
				return getEffects(args[0]);
			} else if (args.length == 2) {
				return getTypes(args[1]);
			}
		}

		return super.tabComplete(sender, alias, args);
	}


	private List<String> getTypes(String s) {
		List<String> strings = new ArrayList<>();
		for(Type p : Type.values()) {
			if(p.toString().toLowerCase().startsWith(s.toLowerCase())) strings.add(p.toString());
		}
		return strings;
	}
	public List<String> getEffects(String s) {
		List<String> strings = new ArrayList<>();
		for(EnumParticle p : EnumParticle.values()) {
			if(p.toString().toLowerCase().startsWith(s.toLowerCase())) strings.add(p.toString());
		}
		return strings;
	}

	@Override
	public boolean execute(CommandSender sender, String command, final String[] args) {

		if(!(sender instanceof Player) ) return true;

		Player p = (Player) sender;
		final StandPlayer sp = StandPlayer.getSP(p.getUniqueId());

		if( args.length == 2) {
			
			sp.playParticle(getParticle(args[0]), Type.getType(args[1]));
			
		} else if(args[0].equalsIgnoreCase("off")) {
			// TODO: Stop particles
		}

		return true;
	}
	
	public EnumParticle getParticle(String s) {
		for(EnumParticle p : EnumParticle.values()) {
			if(p.toString().toLowerCase().startsWith(s.toLowerCase())) return p;
		}
		return null;
	}

}

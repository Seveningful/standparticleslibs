package fr.standblock.hub;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.standblock.hub.manager.LocationManager;
import fr.standblock.hub.particles.Halo;
import fr.standblock.hub.particles.Helix;
import fr.standblock.hub.particles.HelixGround;
import fr.standblock.hub.particles.HelixMoving;
import fr.standblock.hub.particles.Ring;
import fr.standblock.hub.particles.Shower;
import fr.standblock.hub.updater.Updater;

public class Main extends JavaPlugin{

	@Override
	public void onEnable() {

		((CraftServer) this.getServer()).getCommandMap().register("test", new ParticleCommand("test"));
		new Updater(this);
		
		
		/*RegisterEvents*/
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new HelixMoving(), this);
		pm.registerEvents(new Helix(), this);
		pm.registerEvents(new HelixGround(), this);
		pm.registerEvents(new Halo(), this);
		pm.registerEvents(new Ring(), this);
		pm.registerEvents(new Shower(), this);
		pm.registerEvents(new LocationManager(), this);
		super.onEnable();
	}
	
}

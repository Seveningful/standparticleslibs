package fr.standblock.hub;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.Packet;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import fr.standblock.hub.manager.ParticleManager;
import fr.standblock.hub.particles.Type;

public class StandPlayer {
	static Map<UUID ,StandPlayer> players = new HashMap<UUID, StandPlayer>();
	
	UUID id;
	
	public StandPlayer(UUID p) {
		this.id = p;
		players.put(p, this);
	}
	
	public static StandPlayer getSP(UUID id) {
		return players.get(id) == null ? new StandPlayer(id) : players.get(id);
	}
	
	public void playParticle(EnumParticle particle, Type type) {
		/*if(algo == null) {
			algo = new Algorythm(type, particle, this);
			algo.play();
		} else {
			algo.cancel();
			algo.particle = particle;
			algo.type = type;
			algo.play();
		}*/
		ParticleManager.addEffect(id,particle,type);
	}
	
	public Player getPlayer() {
		return Bukkit.getPlayer(id);
	}
	
	public static void sendPacket(Packet<?> packet) {
		for(Player p  : Bukkit.getOnlinePlayers() ) {
			((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
		}
	}


}

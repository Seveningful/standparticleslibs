package fr.standblock.hub.manager;

import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

import fr.standblock.hub.particles.Type;
import net.minecraft.server.v1_8_R3.EnumParticle;

public class ParticleManager {

	static HashMap<UUID, Type> particlesform = new HashMap<>();

	
	public static Set<UUID> getParticlesForm() {
		return particlesform.keySet();
	}
	static public Type getParticlesForm(UUID id) {
		return particlesform.get(id);
	}
	
	public static  Set<UUID> getParticlestype() {
		return particlestype.keySet();
	}
	
	static HashMap<UUID, EnumParticle> particlestype = new HashMap<>();
	
	static public EnumParticle getParticleType(UUID id) {
		return particlestype.get(id);
	}
	public static void addEffect(UUID id, EnumParticle particle, Type type) {
		particlesform.put(id, type);;
		particlestype.put(id, particle);
		
	}
	
}

package fr.standblock.hub.manager;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.standblock.hub.StandPlayer;
import fr.standblock.hub.updater.SchedulerEvent;
import fr.standblock.hub.updater.UpdateType;

public class LocationManager implements Listener{
	static HashMap<UUID, Location> location = new HashMap<>();
	static HashMap<UUID, Location> seclocation = new HashMap<>();
	@EventHandler
	  public void updateEvent(SchedulerEvent e)
	  {
	    if (e.getType() == UpdateType.FASTEST) {
	      for (UUID p : ParticleManager.getParticlesForm()) {
	        if (Bukkit.getPlayer(p) != null )
	        {	
	        	Player player = Bukkit.getPlayer(p);
	        	if(location.get(p) == null) {
	        		location.put(p, player.getLocation());
	        	}else {
	        		seclocation.put(p, location.get(p));
	        		location.put(p, player.getLocation());
	        	}
	        	
	        }
	      }
	    }
	  }
	
	public static boolean isMoving(UUID id) {
		Location loc1 = StandPlayer.getSP(id).getPlayer().getLocation();
		Location loc2 = seclocation.get(id);
		if(loc2 == null) return false;
		return loc1.getX() != loc2.getX() || loc1.getY() != loc2.getY() || loc1.getZ() != loc2.getZ();
	}
}

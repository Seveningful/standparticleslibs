package fr.standblock.hub.particles;

import java.util.UUID;

public interface IEffect {
	public abstract void postPlayEffect();
	public abstract void playEffect(UUID id);
	public abstract void prePlayEffect();
}

package fr.standblock.hub.particles;

import java.util.UUID;

import org.bukkit.scheduler.BukkitRunnable;

import fr.standblock.hub.Main;
import fr.standblock.hub.StandPlayer;
import fr.standblock.hub.manager.ParticleManager;
import fr.standblock.hub.updater.UpdateType;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

public class Helix extends Effect{

	public Helix() {
		super(Type.HELIX, UpdateType.TICK);
	}

	@Override
	public void playEffect(UUID id) {
		final EnumParticle particle = ParticleManager.getParticleType(id);
		final StandPlayer sp = StandPlayer.getSP(id);
		/*
		 * Mesh 1
		 */
		new BukkitRunnable() {
			@Override
			public void run() {
				double delay = 0;
				double space = 0;
				boolean switcher = false;
				for(float y = -3 ; y <=3 ; y+=0.01) {
					delay++;
					if(space >= 1.5) switcher = true;
					if(!switcher) space +=0.005; else space -=0.005;
					if(!(delay%10 == 0)) continue;
					double exactX = (double)space * Math.cos(y);
					double exactZ = - ((double)space * Math.sin(y) );
					
					final double locX = sp.getPlayer().getLocation().getX() + exactZ;
					final double locY = sp.getPlayer().getLocation().getY() + y + 1;
					final double locZ = sp.getPlayer().getLocation().getZ() + exactX;

					new BukkitRunnable() {
						PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, (float)locX , (float)locY, (float)locZ , 
								0F, 0F, 0F, 
								0, 10, 10);
						@Override
						public void run() {

							StandPlayer.sendPacket(packet);

						}
					}.runTaskLater(Main.getPlugin(Main.class), (long) (delay / 10) );
				}

			}
		}.runTaskAsynchronously(Main.getPlugin(Main.class));

		/*
		 * Mesh 2
		 */

		new BukkitRunnable() {
			@Override
			public void run() {
				double delay = 0;
				double space = 0;
				boolean switcher = false;
				for(float y = -3 ; y <=3 ; y+=0.01) {
					delay++;
					if(space >= 1.5) switcher = true;
					if(!switcher) space +=0.005; else space -=0.005;
					if(!(delay%10 == 0)) continue;
					double exactX = (double)space * Math.cos(y);
					double exactZ = - ((double)space * Math.sin(y) );
					final double locX = sp.getPlayer().getLocation().getX() - exactZ;
					final double locY = sp.getPlayer().getLocation().getY() + y + 1;
					final double locZ = sp.getPlayer().getLocation().getZ() - exactX;
					new BukkitRunnable() {
						PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, (float)locX, (float)locY, (float)locZ, 
								0F, 0F, 0F, 
								0, 10, 10);
						@Override
						public void run() {

							StandPlayer.sendPacket(packet);

						}
					}.runTaskLater(Main.getPlugin(Main.class), (long) (delay / 10));
				}
				
			}
		}.runTaskAsynchronously(Main.getPlugin(Main.class));
	
	
	}

	@Override
	public void postPlayEffect() {
		
	}
	
	@Override
	public void prePlayEffect() {
		// TODO Auto-generated method stub
		
	}

}

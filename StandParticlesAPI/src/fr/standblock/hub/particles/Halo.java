package fr.standblock.hub.particles;

import java.util.UUID;

import org.bukkit.scheduler.BukkitRunnable;

import fr.standblock.hub.Main;
import fr.standblock.hub.StandPlayer;
import fr.standblock.hub.manager.ParticleManager;
import fr.standblock.hub.updater.UpdateType;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

public class Halo extends Effect{
	public Halo() {
		super(Type.HALO, UpdateType.FASTER);
	}

	@Override
	public void playEffect(UUID id) {
		int delay = 0;
		StandPlayer sp = StandPlayer.getSP(id);
		EnumParticle particle = ParticleManager.getParticleType(id);
		
		for(float y = 0 ; y <=6.1 ; y+=0.5) {
			
			double exactX = (double)0.5 * Math.cos(y);
			double exactZ = (double)0.5 * Math.sin(y);
			final double locX = sp.getPlayer().getLocation().getX() - exactX;
			final double locY = sp.getPlayer().getEyeLocation().getY() + 0.6;
			final double locZ = sp.getPlayer().getLocation().getZ() - exactZ;
			final PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, (float)locX, (float)locY, (float)locZ, 
					0F, 0F, 0F, 
					0, 1, 1);
			delay++;
			new BukkitRunnable() {
				
				@Override
				public void run() {
					StandPlayer.sendPacket(packet);
					
				}
			}.runTaskLaterAsynchronously(Main.getPlugin(Main.class), delay);
					
					
		}
	}

	@Override
	public void postPlayEffect() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prePlayEffect() {
		// TODO Auto-generated method stub
		
	}
}

package fr.standblock.hub.particles;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import fr.standblock.hub.Main;
import fr.standblock.hub.StandPlayer;
import fr.standblock.hub.manager.LocationManager;
import fr.standblock.hub.manager.ParticleManager;
import fr.standblock.hub.updater.UpdateType;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

public class Ring extends Effect {

	public Ring() {
		super(Type.RING, UpdateType.TICK);
	}

	float 	radius = 1F;
	int 	heigth = 2;
	double constant = 0;
	float 	angle = 1F;
	int amount = 30;
	boolean mode;
	
	@Override
	public void postPlayEffect() {
		getLocations().clear();
	}

	@Override
	public void prePlayEffect() {

		double increment = (2 * Math.PI) / amount;
		double constant = 0;
		for(int i = 0;i < amount; i++)
		{
			double angle = i * increment;
			double exactX = (radius * Math.cos(angle));

			if(mode) {

				constant += heigth/15D;
				mode = constant< heigth;
			} else {
				constant -= heigth/15D;
				mode = constant < 0;
			}

			double exactY = constant;
			double exactZ = (radius * Math.sin(angle));
			addLocation(new Location(Bukkit.getWorlds().get(0), exactX,exactY, exactZ));
			addLocation(new Location(Bukkit.getWorlds().get(0), -exactX,exactY, -exactZ));
		}





	}

	@Override
	public void playEffect(final UUID id) {
		final EnumParticle particle = ParticleManager.getParticleType(id);

		final StandPlayer sp = StandPlayer.getSP(id);
		Location l = sp.getPlayer().getLocation();
		if(LocationManager.isMoving(id)) {

			final PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, (float)l.getX()   , (float)l.getY(), (float)l.getZ(), 
					0F, 0F, 0F, 
					0, 10, 10);
			StandPlayer.sendPacket(packet);
			return;
		}
			long delay = 0;
				

				for(final Location loc : getLocations()) {

					delay ++;

					new BukkitRunnable() {

						@Override
						public void run() {
							Location sploc = sp.getPlayer().getLocation().clone().add(loc.getX(), loc.getY(), loc.getZ());
							final PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, (float)sploc.getX(), (float)sploc.getY(), (float)sploc.getZ(), 
									0F, 0F, 0F, 
									0, 1, 1);
							StandPlayer.sendPacket(packet);

						}
					}.runTaskLaterAsynchronously(Main.getPlugin(Main.class), delay / 10);

				}


	}

}

package fr.standblock.hub.particles;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import fr.standblock.hub.manager.ParticleManager;
import fr.standblock.hub.updater.SchedulerEvent;
import fr.standblock.hub.updater.UpdateType;

public abstract class Effect implements Listener,IEffect {

	
	List<Location> locations = new ArrayList<Location>();
	Type type ;
	UpdateType utype = UpdateType.TICK;
	
	public Effect(Type type) {
		setType(type);

	}
	public Effect(Type type, UpdateType sec) {
		setType(type);
		setUtype(sec);
	}
	
	
	public void setType(Type type) {
		this.type = type;
	}
	
	public Type getType() {
		return type;
	}
	
	public void setUtype(UpdateType utype) {
		this.utype = utype;
	}
	
	public UpdateType getUtype() {
		return utype;
	};
	
	public void addLocation(Location loc) { locations.add(loc); }

	public List<Location> getLocations() {
		return locations;
	}
	@EventHandler
	public void playParticle(SchedulerEvent e) {
		if(e.getType() == utype) {
			prePlayEffect();
			for(UUID id : ParticleManager.getParticlesForm()) {
				if(ParticleManager.getParticlesForm(id) == type) {
					
					playEffect(id);
				}
			}
			postPlayEffect();
		}
	}
	
}

package fr.standblock.hub.particles;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Location;

import fr.standblock.hub.StandPlayer;
import fr.standblock.hub.manager.LocationManager;
import fr.standblock.hub.manager.ParticleManager;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

public class HelixGround extends Effect{
	public HelixGround() {
		super(Type.HELIX_GROUND);
	}
	float rayonDistance = 0.6F;
	float particleRotation = 0.2F;
	double count = 0.0D;

	@Override
	public void postPlayEffect() {

		if (count >= 30.0D) {
			count = 0.0D;
		}
		count += particleRotation;
		
	}
	@Override
	public void playEffect(UUID id) {

		StandPlayer sp = StandPlayer.getSP(id);
		EnumParticle particle = ParticleManager.getParticleType(id);
		Location l = sp.getPlayer().getLocation();
		
		if(LocationManager.isMoving(id)) {

			final PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, (float)l.getX()   , (float)l.getY(), (float)l.getZ(), 
					0F, 0F, 0F, 
					0, 10, 10);
			StandPlayer.sendPacket(packet);
		} else {
			
			

			double m = count;
			double i = 1.5D;

			ArrayList<Location> points2 = new ArrayList<Location>();
			ArrayList<Location> points = new ArrayList<Location>();
			for (;;)
			{
				double nombre2 = Math.PI + m * Math.PI / 7.0D;
				Location loc2 = new Location( sp.getPlayer().getWorld(), l.getX() + 
						Math.cos(nombre2 *rayonDistance) * 
						i, l.getY() , l.getZ() + 
						Math.sin(nombre2 * rayonDistance) * 
						i);
				Location loc3 = new Location(sp.getPlayer().getWorld(), l.getX() + 
						Math.cos(nombre2 * rayonDistance) * 
						-i, l.getY() , l.getZ() + 
						Math.sin(nombre2 * rayonDistance) * 
						-i);

				points2.add(loc2);
				points.add(loc3);
				if (m >= 36.0D + count) {
					break;
				}
				i -= 0.04D;
				m += 1.0D;
			}
			for (Location l4 : points2)
			{
				final PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, (float)l4.getX()   , (float)l4.getY(), (float)l4.getZ(), 
						0F, 0F, 0F, 
						0, 10, 10);
				StandPlayer.sendPacket(packet);
			}
			for (Location l3 : points)
			{
				final PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particle, true, (float)l3.getX()   , (float)l3.getY(), (float)l3.getZ(), 
						0F, 0F, 0F, 
						0,10, 10);
				StandPlayer.sendPacket(packet);
			}
		}
	}
	@Override
	public void prePlayEffect() {
		// TODO Auto-generated method stub
		
	}
}

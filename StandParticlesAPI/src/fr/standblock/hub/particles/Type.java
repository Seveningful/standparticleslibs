package fr.standblock.hub.particles;

public enum Type {

	HELIX,
	HALO, HELIX_MOVING, HELIX_GROUND, RING, RING_DOUBLE, SHOWER,
	CLOUD_GROUND;
	
	public static Type getType(String s) {
		for(Type p : Type.values()) {
			if(p.toString().toLowerCase().startsWith(s.toLowerCase())) return p;
		}
		return null;
	}
	
}

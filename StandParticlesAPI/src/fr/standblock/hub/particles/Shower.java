package fr.standblock.hub.particles;

import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import fr.standblock.hub.Main;
import fr.standblock.hub.StandPlayer;
import fr.standblock.hub.manager.ParticleManager;
import fr.standblock.hub.updater.UpdateType;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

public class Shower extends Effect{
	/**
	 * @author Gui Rodrigues (Seveingful)
	 * 
	 * Displays Shower Effect
	 */
	public Shower() {
		super(Type.SHOWER,UpdateType.TICK);
	}

	float 	radius = 1.2F;
	float	heigth = 2.1F;
	

	@Override
	public void postPlayEffect() {
		getLocations().clear();

	}

	@Override
	public void playEffect(UUID id) {

		final StandPlayer sp = StandPlayer.getSP(id);
		long delay = 0;
		for(final Location loc : getLocations()) {

			delay ++;

			new BukkitRunnable() {

				@Override
				public void run() {
					Location sploc = sp.getPlayer().getLocation().clone().add(loc.getX(), loc.getY() +heigth, loc.getZ());
					final PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(EnumParticle.DRIP_WATER, true, (float)sploc.getX(), (float)sploc.getY(), (float)sploc.getZ(), 
							0F, 0F, 0F, 
							0, 1, 1);
					StandPlayer.sendPacket(packet);
					final PacketPlayOutWorldParticles packet2 = new PacketPlayOutWorldParticles(EnumParticle.CLOUD, true, (float)sploc.getX(), sp.getPlayer().getWorld().getHighestBlockYAt(sploc) , (float)sploc.getZ(), 
							0F, 0F, 0F, 
							0, 1, 1);
					StandPlayer.sendPacket(packet2);


				}
			}.runTaskLaterAsynchronously(Main.getPlugin(Main.class), delay / 10);



		}


	}

	@Override
	public void prePlayEffect() {

		for (float iradius = 0; iradius < radius; iradius +=0.1) {

			int amount = (int) ((new Random().nextInt(30) + 1) * iradius /2);
			double increment = (2 * Math.PI) / amount;

			for (int i = 0; i < amount; i++) {

				double angle = i * increment;
				double exactX = (iradius * Math.cos(angle));
				double exactZ = (iradius * Math.sin(angle));
				addLocation(new Location(Bukkit.getWorlds().get(0), exactX, new Random().nextDouble() /4, exactZ));

			}



		}



	}

}
